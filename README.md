# Are You Mocking Me ?!

* requires PHP 8.1
* a docker container is provided to run integration as unit tests
* test cases can be found under `tests/`
* note there will be some incomplete, failing tests as part of the talk


## Episodes

### Episode I: Yesterday's Code

[Episode I: Yesterday's Code](episodes/episode-I-yesterdays-code.md)

### Episode II: Calamity Workshop

[Episode II: Calamity Workshop](episodes/episode-II-calamity-workshop.md)

### Episode III: Fifty Fixture

[Episode III: Fifty Fixture](episodes/episode-III-fifty-fixture.md)

## Run tests

### Native with php 8.1  

```bash
# setup
composer install

# run all (runnable) tests
./vendor/bin/phpunit --testsuite=final
./vendor/bin/phpunit --testsuite=unit
./vendor/bin/phpunit --testsuite=integration

# tests to complete in "Calamity Workshop"
./vendor/bin/phpunit --testsuite=calamity 
```

### Docker 

1. Running it with `kevin`
    ```bash
    kevin up -d
   
    # shell access to container (not required)
    kevin ssh
    
    kevin composer install
    
    # run all (runnable) tests
    kevin phpunit --testsuite=final
    kevin phpunit --testsuite=unit
    kevin phpunit --testsuite=integration
    
    # test to complete in "Calamity Workshop"
    kevin phpunit --testsuite=calamity
    ```
1. Running it all from within the container 
    ```bash
    # spinning up the container
    docker compose up -d
    
    # shell access to the container
    docker-compose exec -u app "application" bash
    
    composer install
    
    # run all (runnable) tests
    ./vendor/bin/phpunit --testsuite=final
    ./vendor/bin/phpunit --testsuite=unit
    ./vendor/bin/phpunit --testsuite=integration
    
    # tests to complete in "Calamity Workshop"
    ./vendor/bin/phpunit --testsuite=calamity
    ```
1. Running it as one time command with  `docker-compose` or `kevin`
    ```bash
    docker-compose run --rm application composer install
    
    # run all (runnable) tests
    docker-compose run --rm application ./vendor/bin/phpunit --testsuite=final
    docker-compose run --rm application ./vendor/bin/phpunit --testsuite=unit
    docker-compose run --rm application ./vendor/bin/phpunit --testsuite=integration
    
    # test to complete in "Calamity Workshop"
    docker-compose run --rm application ./vendor/bin/phpunit --testsuite=calamity
    ```
