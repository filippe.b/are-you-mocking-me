# Episode I: Yesterday's Code

Presentation of Unit and Integration testing with PHPUnit and Prophecy

* presentation slideds: [are-you-mocking-me.odp](../docs/are-you-mocking-me.odp)
* presentation pdf: [presentation-are-you-mocking-me.pdf](../docs/presentation-are-you-mocking-me.pdf)

```bash
./vendor/bin/phpunit --testsuite=final
./vendor/bin/phpunit --testsuite=unit
./vendor/bin/phpunit --testsuite=integration
```