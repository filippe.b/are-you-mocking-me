# Episode II: Calamity Workshop

Practical approach on how to start on a test case.    
Approach: `tests/HowToStart/HowToStartTest.php`

Complete the test cases in `tests/EpisodeTwoCalamityWorkshop`

```bash
# tests to complete in "Calamity Workshop"
./vendor/bin/phpunit --testsuite=calamity
```