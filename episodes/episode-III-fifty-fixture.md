# Episode III: Fifty Fixture

Facilitate tests that require Model data in a specific state.  
Often service test cases (repository, collection, mappers, ...)  will require a specific **model state**.  
Creating each time a new model instance with unique data can become quite cumbersome.

Furthermore, when a model is modified, the widespread of model usage needs to be maintained as well,  
making it a more tedious and time-consuming task.

Hence: the introduction of **Model Fixtures**

### Purpose

1. Ease-of-use of model class instantiation during testing
2. Central place to update test model states when model is modified
   (without a widespread of model use cases that need adjustments)

### Best Practice

* Only to be used in testing scope
* Never to be used in production code
* Lives ideally under the `test` namespace
* At the core: a simple, flexible Factory pattern
* Do not abuse the `default` method for specific cases (insert, update, api-request, ...)
* **Tip**: Avoid the usage in its own Model unit test  
  *Theoretically possible, but the unit-test should directly test the model and not go through a fixture*


### Improved Repository / External source testing

Additionally: the Repository integration tests becomes more robust   
with the combination of **model fixture** and using the **mapper** implementation
1. **loading** of database fixtures: mapping the model fixture to the data-source 
2. **fetching**: mapping data from the data-source to a model  
   * to ease the use of fetch by id a helper method  `private fetch(string $id)` is added which makes use of the mapper 

This ensures the mapper is capable of mapping correctly to an actual integration resource,  
which the unit test does not cover.  
Example: `tests/EpisodeThreeFiftyFixture/Integration/Database/Repository/BrandRepositoryTest.php`

## Run Tests that use model fixtures

```bash

# tests to run complete in "Fifty Fixture"
./vendor/bin/phpunit --testsuite=fifty-fixture
```

## Structure

```bash
are-you-mocking-me/
├─ src/
│  ├─ Model/
|  |  └─ Brand.php
│  ├─ Collection/
│  │  └─ BrandCollection.php
│  └─ Database/
|     ├─ Mapper/
|     |  └─ BrandMapper.php
|     └─ Repository/
|        └─ BrandRepository.php
|
└─ tests/EpisodeThreeFiftyFixture/
   ├─ Fixtures/
   │  └─ Model/
   │     └─ ProductBrandFixture.php
   |
   ├─ Integration
   |  └─ Database
   |     ├─ Mapper
   |     |  └─ BrandMapperTest.php                 uses model fixtures
   |     └─ Repository
   |        └─ Repository
   |           └─ BrandRepositoryTest.php          uses model fixtures and mapper
   |  
   └─ Unit
      ├─ Collection/
      |  ├─ BrandCollectionTest.php                uses model fixtures
      |  └─ BrandCollectionWithoutFixtureTest.php  without model fixtures as a comparison
      ├─ Mapper/
      |  └─ BrandMapperTest.php                    uses model fixtures
      └─ Model/
         └─ BrandTest.php                          should *not* use model fixtures
```
