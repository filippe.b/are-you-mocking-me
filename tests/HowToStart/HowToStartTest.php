<?php

declare(strict_types=1);

/**
 * 1. Use the same namespace as the "class" you want to test,
 * prefixed with the test Namespace defined in composer.json
 * current case: Aymm\HowToStart -> Aymm\Tests\HowToStart
 */

namespace Aymm\Tests\HowToStart;

use Aymm\HowToStart\HowToStart;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;

/**
 * 2. Add "Test" Suffix to the class name
 * current: "HowToStart" + "Test"
 *
 * 3. extend PHPUnit TestCase
 */
class HowToStartTest extends TestCase
{
    use ProphecyTrait;

    private HowToStart $howToStart;

    /**
     * 4. Most test will start with a setUp(),
     * executed before each test method contained in this class
     */
    protected function setUp(): void
    {
        /**
         * - Define common tasks
         * - Initialise the class (Model, service, ...) you want to test
         * - if needed mock injected services with prophecy (requires)
         * - store them in data-members when needing access in test methods
         */

        // Mock the logger interface, we don't test injected services, only that they are called
        $this->logger = $this->prophesize(LoggerInterface::class);

        $this->howToStart = new HowToStart(
            $this->logger->reveal(), // use reveal on a Prohpecy object to reveal the actual class (logger)
            'Harrison Ford' // use actual data as much as possible
        );
    }


    /**
     * 5. Create test methods
     * - prefix name with "test" or annotate with "@test"
     * - All public methods should be tested
     * - protected and private methods should be tested through the public calling them
     * - use a method name that describes what will be tested and what is expected
     *  example: it_contains_a_name to test an accessor "name" of a Model / Entity class
     *
     * @test
     */
    public function it_contains_a_name(): void
    {
        $this->assertEquals('Harrison Ford', $this->howToStart->name());
    }

    /**
     * 6.1 Create separate test methods / cases
     *  - each case has a dedicated test method even (don't combine multiple cases)
     *  - a good example: early returns
     */
    /** @test */
    public function it_does_not_log_an_empty_name(): void
    {
        /**
         * Building a test case / method
         * 1. set up the data to be used in the test case
         * 2. configure the mocked service if required
         * 3. call the method / case we want to test
         * 4. verify (assert) the result is as expected
         */

        // 1. set up the data
        $name = '';
        $howToStart = new HowToStart($this->logger->reveal(), $name);

        /**
         * 2. configure the mocked service if required
         * - Argument::cetera(): "Matches all values to the rest of the signature."
         * - shouldNotBeCalled() -> Predictions it should never be called during this case, if it does, the test fails
         */
        $this->logger->info(Argument::cetera())->shouldNotBeCalled();

        // 3. call the method / case we want to test
        $result = $howToStart->logName();

        // 4. verify (assert) the result is as expected
        $this->assertEquals('', $result);
    }

    /**
     * 6.2 Second case for logging a name method
     * - assert a given name can be logged, and is returned
     * - using "setUp" data
     *
     * @test
     */
    public function it_logs_a_name_and_returns_a_log_message(): void
    {
        /**
         * Verifying the mocked injected service call with Prophecy
         * - Using Argument::that( callback: bool)) to determine the passed arguments are the ones expected
         * - Using Should be called on a void method to ensure it was called
         */
        $expectedMessage = 'Name: Harrison Ford';
        $verifyLogMessage = static function (string $message) use ($expectedMessage): bool {
            return $expectedMessage === $message;
        };
        $this->logger->info(
            Argument::that($verifyLogMessage)
        )->shouldBeCalled();

        // simple types (string, int, ...) can also be validated directly
        $this->logger->info($expectedMessage)->shouldBeCalled();

        // 3. call the method / case we want to test
        $result = $this->howToStart->logName();

        // 4. verify (assert) the result is as expected
        $this->assertEquals($expectedMessage, $result);
    }

    /**
     * 6.3 Third case for logging a name method
     * - asserting that the private method "parses" the name
     *
     * @test
     */
    public function it_parses_the_name_when_logging(): void
    {
        $name = 'edDy loWerCasE naMe';
        $howToStart = new HowToStart($this->logger->reveal(), $name);

        $expectedMessage = 'Name: Eddy Lowercase Name';
        $this->logger->info($expectedMessage)->shouldBeCalled();

        $result = $howToStart->logName();
        $this->assertEquals($expectedMessage, $result);
    }

    /**
     * 6.4 Fourth case for logging a name method
     * - asserting that an exception is thrown for a fake name
     *
     * @test
     */
    public function it_throw_invalid_argument_exception_on_fake_name(): void
    {
        /**
         * Best practice:
         * - as first of declaration: assert exception, visual clear an exception is expected
         * - Always assert the message
         */
        $name = 'fake';
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid name: "'.$name.'"');

        $howToStart = new HowToStart($this->logger->reveal(), $name);

        // Assert the logger is never called
        $this->logger->info(Argument::cetera())->shouldNotBeCalled();

        $howToStart->logName();
    }
}
