<?php

declare(strict_types=1);

namespace Aymm\Tests\Integration\Database;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use PHPUnit\Framework\TestCase;

abstract class DatabaseTestCase extends TestCase
{
    protected ?Connection $connection = null;
    /**
     * @var array<string, array> [tableName => data]
     */
    protected array $fixtures = [];

    protected function setUp(): void
    {
        $this->createConnection();
        $this->ensureSchemaExists();
        $this->removeFixtures();
        $this->provideFixtures();
    }

    protected function tearDown(): void
    {
        $this->removeFixtures();
    }

    protected function createConnection(): Connection
    {
        if ($this->connection) {
            return $this->connection;
        }

        return $this->connection = DriverManager::getConnection([
            'url' => 'mysql://root:toor@db/aymm',
        ]);
    }

    private function ensureSchemaExists(): void
    {
        $schema = file_get_contents(getcwd().'/database/schema.sql');
        $this->connection->executeQuery($schema);
    }

    protected function provideFixtures(): void
    {
        foreach ($this->loadFixtures() as $table => $data) {
            $this->fixtures[] = ['table' => $table, 'data' => $data];
            if ('' === (string) $table) {
                continue;
            }

            $this->connection->insert($table, $data);
        }
    }

    protected function removeFixtures(): void
    {
        foreach ($this->loadFixtures() as $table => $data) {
            if ('' === (string) $table) {
                continue;
            }
            $this->connection->executeQuery('TRUNCATE TABLE `'.$table.'`;');
        }
    }

    /**
     * @var array<string, array> [tableName => data]
     */
    abstract protected function loadFixtures(): \Generator;
}
