<?php

declare(strict_types=1);

namespace Aymm\Tests\Integration\Database\Repository;

use Aymm\Collection\CompanyCollection;
use Aymm\Database\Mapper\CompanyMapper;
use Aymm\Database\Repository\CompanyRepository;
use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Aymm\Model\CompanyState;
use Aymm\Tests\Integration\Database\DatabaseTestCase;

final class CompanyRepositoryTest extends DatabaseTestCase
{
    private CompanyRepository $repository;

    protected function setUp(): void
    {
        $this->repository = new CompanyRepository(
            $this->createConnection(),
            new CompanyMapper()
        );

        parent::setUp();
    }

    /** @test */
    public function it_can_fetch_by_id(): void
    {
        $result = $this->repository->fetchById(
            CompanyId::fromString($id = '912c8327-4680-453d-9ef2-580e2622e20b')
        );

        $this->assertInstanceOf(Company::class, $result);
        $this->assertEquals($id, $result->companyId->id());
        $this->assertEquals('Reynholm Industries', $result->name);
        $this->assertEquals(true, $result->active);
        $this->assertEquals(CompanyState::CONFIRMED, $result->companyState);
    }

    /** @test */
    public function it_can_fetch_active(): void
    {
        $result = $this->repository->fetchActive();

        $this->assertInstanceOf(CompanyCollection::class, $result);
        $this->assertCount(2, $result);
        $this->assertEquals(
            [
                '8c0181f7-a1ec-4310-b7d2-bb717f1bdcfb',
                '912c8327-4680-453d-9ef2-580e2622e20b',
            ],
            $result->map(static fn(Company $company) => $company->companyId->id())
        );
    }

    /** @test */
    public function it_can_create_a_company(): void
    {
        $company = Company::create(
            CompanyId::fromString($id = 'c393d7f1-bf8c-45ce-92b0-76fce60e245d'),
            'Moss\'s Endeavour',
            CompanyState::PENDING
        );

        $this->repository->create($company);
        $result = $this->connection->fetchAssociative(
            'SELECT id FROM company WHERE id = :companyId',
            ['companyId' => $id]
        );
        $this->assertNotFalse($result);
        $this->assertCount(1, $result);
        $this->assertEquals([ 'id' => $id], $result);
    }

    protected function loadFixtures(): \Generator
    {
        yield 'company' => [
            'id' => '912c8327-4680-453d-9ef2-580e2622e20b',
            'name' => 'Reynholm Industries',
            'active' => 1,
            'state' => CompanyState::CONFIRMED->value,
        ];

        yield 'company' => [
            'id' => '8c0181f7-a1ec-4310-b7d2-bb717f1bdcfb',
            'name' => 'Calamity Industries',
            'active' => 1,
            'state' => CompanyState::CONFIRMED->value,
        ];

        yield 'company' => [
            'id' => 'c4a5619c-c80c-4d7a-977c-3e2f0d99e105',
            'name' => 'Out of Commision NV',
            'active' => 0,
            'state' => CompanyState::CONFIRMED->value,
        ];
    }
}
