<?php

declare(strict_types=1);

namespace Aymm\Tests\Integration\Database\Provider\Employee;

use Aymm\Collection\EmployeeIdCollection;
use Aymm\Database\Provider\Employee\ActiveIdProvider;
use Aymm\Tests\Integration\Database\DatabaseTestCase;
use Ramsey\Uuid\UuidInterface;

final class ActiveIdProviderTest extends DatabaseTestCase
{
    private ActiveIdProvider $provider;

    protected function setUp(): void
    {
        $this->provider = new ActiveIdProvider(
            $this->createConnection()
        );

        parent::setUp();
    }

    /** @test */
    public function it_can_provide(): void
    {
        $result = $this->provider->provide();

        $this->assertInstanceOf(EmployeeIdCollection::class, $result);
        $this->assertCount(2, $result);
        $this->assertEquals([
            'b65d7360-f706-4838-99f6-7c80bf552799',
            'f99fd931-8ad0-4175-b601-40a7fd685de8',
        ], $result->map(static fn (UuidInterface $id): string => $id->toString()));
    }

    protected function loadFixtures(): \Generator
    {
        yield 'employee' => [
            'id' => 'f99fd931-8ad0-4175-b601-40a7fd685de8',
            'active' => 1,
            'first_name' => 'Roy',
            'last_name' => 'Trenneman',
            'created_at' => (new \DateTimeImmutable('-15 days 06:00:00'))->format(\DateTimeInterface::ATOM),
            'updated_at' => (new \DateTimeImmutable('-13 days 08:00:00'))->format(\DateTimeInterface::ATOM),
        ];

        yield 'employee' => [
            'id' => 'b65d7360-f706-4838-99f6-7c80bf552799',
            'active' => 1,
            'first_name' => 'Jen',
            'last_name' => 'Barber',
            'created_at' => (new \DateTimeImmutable('-5 days 06:00:00'))->format(\DateTimeInterface::ATOM),
            'updated_at' => (new \DateTimeImmutable('-3 days 08:00:00'))->format(\DateTimeInterface::ATOM),
        ];

        yield 'employee' => [
            'id' => 'ecc63ee9-e3d1-45ad-aa9b-1d8627b4ae0b',
            'active' => 0,
            'first_name' => 'Richmond',
            'last_name' => 'Avenal',
            'created_at' => (new \DateTimeImmutable('-30 days 06:00:00'))->format(\DateTimeInterface::ATOM),
            'updated_at' => (new \DateTimeImmutable('-25 days 08:00:00'))->format(\DateTimeInterface::ATOM),
        ];
    }
}
