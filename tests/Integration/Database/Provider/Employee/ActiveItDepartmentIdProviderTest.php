<?php

declare(strict_types=1);

namespace Aymm\Tests\Integration\Database\Provider\Employee;

use Aymm\Collection\EmployeeIdCollection;
use Aymm\Database\Provider\Employee\ActiveItDepartmentIdProvider;
use Aymm\Tests\Integration\Database\DatabaseTestCase;

final class ActiveItDepartmentIdProviderTest extends DatabaseTestCase
{
    private ActiveItDepartmentIdProvider $provider;

    public function setUp(): void
    {
        $this->provider = new ActiveItDepartmentIdProvider(
            $this->createConnection()
        );
    }

    /** @test */
    public function it_can_provide(): void
    {
        $result = $this->provider->provide();

        $this->assertInstanceOf(EmployeeIdCollection::class, $result);
        $this->assertCount(0, $result);
    }

    protected function loadFixtures(): \Generator
    {
        yield '' => [];
    }
}
