<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeThreeFiftyFixture\Unit\Collection;

use Aymm\Collection\BrandCollection;
use Aymm\Tests\EpisodeThreeFiftyFixture\Fixtures\Model\BrandFixture;
use PHPUnit\Framework\TestCase;

/**
 * Ideal case to use Model fixture:
 *
 * the collection unit test scope is to test the collection,
 * not the underlying models it requires
 */
final class BrandCollectionTest extends TestCase
{
    private BrandCollection $collection;

    protected function setUp(): void
    {
        $this->collection = new BrandCollection(
            BrandFixture::default('6527fb53-e3d8-4749-8b01-fbf1d0fe9a7b'),
            BrandFixture::default('a102ffdd-7c6f-48f8-8f17-e8459c78c259'),
        );
    }

    /** @test */
    public function it_is_an_iterator_aggregate(): void
    {
        $this->assertInstanceOf(\IteratorAggregate::class, $this->collection);
    }

    /**
     * Model fixtures greatly simplify this collection test case
     *
     * @see BrandCollectionWithoutFixtureTest::it_can_iterate()
     *
     * @test
     */
    public function it_can_iterate(): void
    {
        $this->assertEquals([
            BrandFixture::default('6527fb53-e3d8-4749-8b01-fbf1d0fe9a7b'),
            BrandFixture::default('a102ffdd-7c6f-48f8-8f17-e8459c78c259'),
        ], \iterator_to_array($this->collection));
    }

    /** @test */
    public function it_is_countable(): void
    {
        $this->assertInstanceOf(\Countable::class, $this->collection);
    }

    /** @test */
    public function it_can_count(): void
    {
        $this->assertCount(2, $this->collection);
    }
}
