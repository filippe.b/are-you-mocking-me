<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeThreeFiftyFixture\Unit\Collection;

use Aymm\Collection\BrandCollection;
use Aymm\Model\Brand;
use Aymm\Model\BrandId;
use PHPUnit\Framework\TestCase;

/**
 * Collection test without Model fixture
 */
final class BrandCollectionWithoutFixtureTest extends TestCase
{
    private BrandCollection $collection;

    protected function setUp(): void
    {
        $this->collection = new BrandCollection(
            Brand::load(
                BrandId::fromString('6527fb53-e3d8-4749-8b01-fbf1d0fe9a7b'),
                'uid-one',
                'name-one',
                new \DateTimeImmutable('06:00:00')
            ),
            Brand::load(
                BrandId::fromString('a102ffdd-7c6f-48f8-8f17-e8459c78c259'),
                'uid-two',
                'name-two',
                new \DateTimeImmutable('06:00:00')
            ),
        );
    }

    /** @test */
    public function it_is_an_iterator_aggregate(): void
    {
        $this->assertInstanceOf(\IteratorAggregate::class, $this->collection);
    }

    /**
     * Model fixtures could simplify this case
     *
     * @see BrandCollectionTest::it_can_iterate()
     *
     * @test
     */
    public function it_can_iterate(): void
    {
        $this->assertEquals([
            Brand::load(
                BrandId::fromString('6527fb53-e3d8-4749-8b01-fbf1d0fe9a7b'),
                'uid-one',
                'name-one',
                new \DateTimeImmutable('06:00:00')
            ),
            Brand::load(
                BrandId::fromString('a102ffdd-7c6f-48f8-8f17-e8459c78c259'),
                'uid-two',
                'name-two',
                new \DateTimeImmutable('06:00:00')
            ),
        ],
            \iterator_to_array($this->collection)
        );
    }

    /** @test */
    public function it_is_countable(): void
    {
        $this->assertInstanceOf(\Countable::class, $this->collection);
    }

    /** @test */
    public function it_can_count(): void
    {
        $this->assertCount(2, $this->collection);
    }
}
