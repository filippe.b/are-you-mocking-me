<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeThreeFiftyFixture\Unit\Model;

use Aymm\Model\BrandId;
use PHPUnit\Framework\TestCase;

final class BrandIdTest extends TestCase
{
    private BrandId $id;

    protected function setUp(): void
    {
        $this->id = BrandId::fromString('5d6f7463-00ab-4edf-869c-e484761a970c');
    }

    /** @test */
    public function it_contains_an_id(): void
    {
        $this->assertInstanceOf(BrandId::class, $this->id);
        $this->assertEquals('5d6f7463-00ab-4edf-869c-e484761a970c', $this->id->id());
    }
}
