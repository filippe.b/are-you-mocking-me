<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeThreeFiftyFixture\Unit\Model;

use Aymm\Model\Brand;
use Aymm\Model\BrandId;
use PHPUnit\Framework\TestCase;

final class BrandTest extends TestCase
{
    private Brand $model;

    protected function setUp(): void
    {
        // Theoretically possible `BrandFixture::default(...)`, but in general not a good practice
        $this->model = Brand::load(
            BrandId::fromString('501dfe28-937a-460d-af2e-c3f2d826bc5c'),
            'brand-uid',
            'brand-name',
            new \DateTimeImmutable('06:00:00')
        );
    }

    /** @test */
    public function it_contains_a_brand_id(): void
    {
        $this->assertEquals('501dfe28-937a-460d-af2e-c3f2d826bc5c', $this->model->brandId->id());
    }

    /** @test */
    public function it_contains_a_brand_uid(): void
    {
        $this->assertEquals('brand-uid', $this->model->uid);
    }

    /** @test */
    public function it_contains_a_brand_name(): void
    {
        $this->assertEquals('brand-name', $this->model->name);
    }

    /** @test */
    public function it_contains_a_brand_updated_at(): void
    {
        $this->assertEquals(new \DateTimeImmutable('06:00:00'), $this->model->updatedAt);
    }
}
