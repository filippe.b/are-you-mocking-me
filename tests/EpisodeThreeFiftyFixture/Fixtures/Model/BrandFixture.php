<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeThreeFiftyFixture\Fixtures\Model;

use Aymm\Model\Brand;
use Aymm\Model\BrandId;

final class BrandFixture
{
    /**
     * Default Test Brand fixture, with flexible options:
     * only set what is required for the use case
     */
    public static function default(
        string $brandId,
        ?string $brandUid = null,
        ?string $brandName = null,
        ?\DateTimeImmutable $updatedAt = null,
    ): Brand {
        return Brand::load(
            BrandId::fromString($brandId),
            $brandUid ?? 'default-brand-uid',
            $brandName ?? 'default-brand-name',
            $updatedAt
        );
    }

    public static function mapToDb(): Brand
    {
        return Brand::load(
            BrandId::fromString('bdfdc296-54af-45bd-8a51-484308fa04c5'),
            'brand-uid-map-to-db',
            'brand-name-map-to-db',
            new \DateTimeImmutable('06:45:00')
        );
    }

    public static function mapFromDb(): Brand
    {
        return Brand::load(
            BrandId::fromString('06712810-435b-4191-abff-8467d02feaa1'),
            'brand-uid-map-from-db',
            'brand-name-map-from-db',
            new \DateTimeImmutable('08:15:00')
        );
    }

    public static function createDb(): Brand
    {
        return Brand::load(
            BrandId::fromString('a930ce24-0469-44ec-9ea1-2bdbee75e32a'),
            'insert-brand-uid',
            'insert-brand-name',
            new \DateTimeImmutable('12:21:00')
        );
    }

    public static function updateDb(): Brand
    {
        return Brand::load(
            BrandId::fromString('d10be53c-19d1-48a0-b0ab-ba6b89ee8c49'),
            'update-brand-uid',
            'update-brand-name',
            new \DateTimeImmutable('18:59:00')
        );
    }

    public static function forApiRequest(): Brand
    {
        return Brand::load(
            BrandId::fromString('87b27f80-1e33-418f-9935-2413787ec271'),
            'actual-uid-for-api-request',
            'actual-name-for-api-request',
            new \DateTimeImmutable('22:25:00')
        );
    }
}
