<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeThreeFiftyFixture\Integration\Database\Repository;

use Aymm\Collection\BrandCollection;
use Aymm\Database\Mapper\BrandMapper;
use Aymm\Database\Repository\BrandRepository;
use Aymm\Model\Brand;
use Aymm\Model\BrandId;
use Aymm\Tests\EpisodeThreeFiftyFixture\Fixtures\Model\BrandFixture;
use Aymm\Tests\Integration\Database\DatabaseTestCase;

final class BrandRepositoryTest extends DatabaseTestCase
{
    private BrandMapper $mapper;
    private BrandRepository $repository;

    protected function setUp(): void
    {
        $this->mapper = new BrandMapper();
        parent::setUp();

        $this->repository = new BrandRepository(
            $this->createConnection(),
            $this->mapper
        );
    }

    /** @test */
    public function it_can_fetch_by_id(): void
    {
        $result = $this->repository->fetchById(
            $id = BrandId::fromString('606b3f52-f18b-40f6-b4ef-fa2f8978e939')
        );

        $this->assertInstanceOf(Brand::class, $result);
        $this->assertEquals($id, $result->brandId);
        $this->assertEquals('96MK78', $result->uid);
        $this->assertEquals('MakikDa', $result->name);
        $this->assertNull($result->updatedAt);
    }

    /** @test */
    public function it_can_fetch_last_updated(): void
    {
        $result = $this->repository->fetchLastUpdated();

        $this->assertInstanceOf(BrandCollection::class, $result);
        $this->assertCount(5, $result);

        $records = \iterator_to_array($result);
        $this->assertEquals('7d59dbb3-588e-40ff-87e8-4cce1f5d6ef2', $records[0]->brandId->id());
        $this->assertEquals('5f21c592-9987-4401-b9e8-d960ac5b382f', $records[1]->brandId->id());
        $this->assertEquals('1138f2ac-bbab-4e32-b8aa-dc915d3e6da2', $records[2]->brandId->id());
        $this->assertEquals('90dc1729-db91-4bfd-827a-426810743250', $records[3]->brandId->id());
        $this->assertEquals('0635e999-e03c-456b-b94c-1544508686af', $records[4]->brandId->id());
    }

    /** @test */
    public function it_can_create_a_brand(): void
    {
        $brand = BrandFixture::createDb();
        $this->repository->create($brand);

        $result = $this->fetch($brand->brandId->id());
        $this->assertInstanceOf(Brand::class, $result);
        $this->assertNotSame($brand, $result);
        $this->assertEquals($brand, $result);
    }

    /** @test */
    public function it_can_update_a_brand(): void
    {
        $brand = BrandFixture::updateDb();
        $this->repository->update($brand);

        $result = $this->fetch($brand->brandId->id());
        $this->assertInstanceOf(Brand::class, $result);
        $this->assertNotSame($brand, $result);
        $this->assertEquals($brand, $result);
    }

    private function fetch(string $brandId): Brand
    {
        return $this->mapper->fromDb(
            $this->connection->fetchAssociative(
                'SELECT * FROM brand WHERE id = "'.$brandId.'"'
            )
        );
    }

    protected function loadFixtures(): \Generator
    {
        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: BrandFixture::updateDb()->brandId->id(),
                brandUid: 'uid-before-update',
                brandName: 'name-before-update',
            )
        );

        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: '606b3f52-f18b-40f6-b4ef-fa2f8978e939',
                brandUid: '96MK78',
                brandName: 'MakikDa',
            )
        );

        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: '0635e999-e03c-456b-b94c-1544508686af',
                brandUid: 'LST963',
                brandName: 'Milkwauwawe',
                updatedAt: new \DateTimeImmutable('-5 days 06:00:00'),
            )
        );

        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: '5f21c592-9987-4401-b9e8-d960ac5b382f',
                brandUid: 'LST963',
                brandName: 'Loosetool',
                updatedAt: new \DateTimeImmutable('-1 day 08:30:00'),
            )
        );

        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: '7d59dbb3-588e-40ff-87e8-4cce1f5d6ef2',
                brandUid: '31754',
                brandName: 'Brennerstuhl',
                updatedAt: new \DateTimeImmutable('-30 min'),
            )
        );

        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: '1138f2ac-bbab-4e32-b8aa-dc915d3e6da2',
                brandUid: 'HIK88746',
                brandName: 'Hokiko',
                updatedAt: new \DateTimeImmutable('-1 day 07:30:00'),
            )
        );

        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: '90dc1729-db91-4bfd-827a-426810743250',
                brandUid: '864471',
                brandName: 'DeWa',
                updatedAt: new \DateTimeImmutable('-3 days 09:30:00'),
            )
        );

        yield 'brand' => $this->mapper->toDb(
            BrandFixture::default(
                brandId: 'c4853df6-6625-4cf1-8cb2-18cc18feaebc',
                brandUid: '864471',
                brandName: 'BlakÄdder',
                updatedAt: new \DateTimeImmutable('-50 days 10:30:00'),
            )
        );
    }
}
