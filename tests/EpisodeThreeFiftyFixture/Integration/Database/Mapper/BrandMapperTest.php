<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeThreeFiftyFixture\Integration\Database\Mapper;

use Aymm\Database\Mapper\BrandMapper;
use Aymm\Model\Brand;
use Aymm\Tests\EpisodeThreeFiftyFixture\Fixtures\Model\BrandFixture;
use PHPUnit\Framework\TestCase;

final class BrandMapperTest extends TestCase
{
    private BrandMapper $mapper;

    protected function setUp(): void
    {
        $this->mapper = new BrandMapper();
    }

    /** @test */
    public function it_can_map_to_db(): void
    {
        $brand = BrandFixture::mapToDb();

        $result = $this->mapper->toDb($brand);
        $this->assertEquals([
            'id' => 'bdfdc296-54af-45bd-8a51-484308fa04c5',
            'uid' => 'brand-uid-map-to-db',
            'name' => 'brand-name-map-to-db',
            'updated_at' => (new \DateTimeImmutable('06:45:00'))->format('Y-m-d H:i:s'),
        ], $result);
    }

    /** @test */
    public function it_can_map_to_db_alternative(): void
    {
        $brand = BrandFixture::default(
            brandId: $brandId = '7b6d325f-7923-4563-8006-6d525710e735',
            brandUid: $brandUid = 'uid-to-db',
            brandName: $brandName = 'name-to-db',
            updatedAt: $updatedAt = new \DateTimeImmutable('06:45:00'),
        );

        $result = $this->mapper->toDb($brand);
        $this->assertEquals([
            'id' => $brandId,
            'uid' => $brandUid,
            'name' => $brandName,
            'updated_at' => $updatedAt->format('Y-m-d H:i:s'),
        ], $result);
    }

    /** @test */
    public function it_can_map_from_db(): void
    {
        $data = [
            'id' => $id = 'c2eb4ef1-51cd-4a86-b0ec-0c4ea1e9813d',
            'uid' => $uid = 'brand-uid-map-from-db',
            'name' => $name = 'brand-name-map-from-db',
            'updated_at' => ($updatedAt = new \DateTimeImmutable('07:45:00'))->format('Y-m-d H:i:s'),
        ];

        $result = $this->mapper->fromDb($data);
        $this->assertInstanceOf(Brand::class, $result);
        $this->assertEquals($id, $result->brandId->id());
        $this->assertEquals($uid, $result->uid);
        $this->assertEquals($name, $result->name);
        $this->assertEquals($updatedAt, $result->updatedAt);
    }

    /** @test */
    public function it_can_map_from_db_alternative(): void
    {
        $data = [
            'id' => '06712810-435b-4191-abff-8467d02feaa1',
            'uid' => 'brand-uid-map-from-db',
            'name' => 'brand-name-map-from-db',
            'updated_at' => (new \DateTimeImmutable('08:15:00'))->format('Y-m-d H:i:s'),
        ];

        $result = $this->mapper->fromDb($data);
        $this->assertInstanceOf(Brand::class, $result);
        $this->assertNotSame(BrandFixture::mapFromDb(), $result);
        $this->assertEquals(BrandFixture::mapFromDb(), $result);
    }
}
