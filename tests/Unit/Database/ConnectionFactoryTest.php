<?php

declare(strict_types=1);

namespace Aymm\Tests\Unit\Database;

use Aymm\Database\ConnectionFactory;
use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;

final class ConnectionFactoryTest extends TestCase
{
    private ConnectionFactory $factory;

    protected function setUp(): void
    {
        $this->factory = new ConnectionFactory;
    }

    /** @test */
    public function it_can_create_a_connection(): void
    {
        $result = $this->factory::create();

        $this->assertInstanceOf(Connection::class, $result);

        $this->assertEquals([
            'url' => 'mysql://root:toor@db/are_you_mocking_me',
            'driver' => 'pdo_mysql',
            'host' => 'db',
            'user' => 'root',
            'password' => 'toor', // should not be in a test this
            'dbname' => 'are_you_mocking_me',
        ], $result->getParams());
    }

    /** @test */
    public function it_can_reuse_existing_connection(): void
    {
        $resultOne = $this->factory::create();
        $resultTwo = $this->factory::create();

        $this->assertSame($resultOne, $resultTwo);
    }
}
