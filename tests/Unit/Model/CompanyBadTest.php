<?php
declare(strict_types=1);

namespace Aymm\Tests\Unit\Model;

use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Aymm\Model\CompanyStatus;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Ramsey\Uuid\UuidInterface;

final class CompanyBadTest extends TestCase
{
    use ProphecyTrait;

    private Company $company;

    protected function setUp(): void
    {
        $uuid = $this->prophesize(CompanyId::class);
        $uuid->id()->willReturn('mocking-caveat-this-not-an-uuid');

        $companyStatus = $this->prophesize(CompanyStatus::class);
        $companyStatus->status()->willReturn('mocking-caveat-this-value-is-never-allowed');

        $this->company = Company::create(
            $uuid->reveal(),
            'IT Crowd',
            $companyStatus->reveal(),
        );
    }

    /** @test */
    public function it_contains_an_id(): void
    {
        $this->assertInstanceOf(CompanyId::class, $this->company->companyId);
        $this->assertEquals('mocking-caveat-this-not-an-uuid', $this->company->companyId->id());
    }

    /** @test */
    public function it_contains_a_name(): void
    {
        $this->assertEquals('IT Crowd', $this->company->name);
    }

    /** @test */
    public function it_contains_an_active_flag(): void
    {
        $this->assertFalse($this->company->active);
    }

    /** @test */
    public function it_contains_a_company_status(): void
    {
        $this->assertInstanceOf(CompanyStatus::class, $this->company->companyState);
        $this->assertEquals(
            'mocking-caveat-this-value-is-never-allowed',
            $this->company->companyState->status()
        );
    }

    /** @test */
    public function it_can_activate(): void
    {
        $newCompanyStatus = $this->prophesize(CompanyStatus::class);
        $newCompanyStatus->status()->willReturn('mocking-caveat-not-valid');

        $result = $this->company->activate($newCompanyStatus->reveal());

        $this->assertInstanceOf(Company::class, $result);
        $this->assertNotSame($this->company, $result);
        $this->assertEquals('mocking-caveat-this-not-an-uuid', $this->company->companyId->id());
        $this->assertEquals('IT Crowd', $result->name);
        $this->assertTrue($result->active);
        $this->assertEquals('mocking-caveat-not-valid', $result->companyState->status());
    }
}
