<?php
declare(strict_types=1);

namespace Aymm\Tests\Unit\Model;

use Aymm\Model\CompanyState;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

final class CompanyStateTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function it_contains_a_status(): void
    {
        $state = CompanyState::PENDING;
        $this->assertEquals('PENDING', $state->state());
    }

    /** @test */
    public function bad_it_contains_a_status(): void
    {
        $state = CompanyState::PENDING;
        $this->assertEquals($state->value, $state->state());
    }

    /** @test */
    public function it_contains_a_value(): void
    {
        $state = CompanyState::CONFIRMED;
        $this->assertEquals('CONFIRMED', $state->value);
    }

    /** @test */
    public function it_contains_a_name(): void
    {
        $state = CompanyState::CONFIRMED;
        $this->assertEquals('CONFIRMED', $state->name);
    }

    /** @test */
    public function it_can_be_constructed_with_from(): void
    {
        $state = CompanyState::from($value = 'STATE_DISABLED');
        $this->assertInstanceOf(CompanyState::class, $state);
        $this->assertEquals('DISABLED', $state->name);
        $this->assertEquals($value, $state->value);
    }

    /** @test */
    public function it_cannot_be_constructed_from_unknown_value(): void
    {
        $this->expectError();
        $this->expectErrorMessage('"UNKNOWN_STATE" is not a valid backing value');

        CompanyState::from($value = 'UNKNOWN_STATE');
    }

    /** @test */
    public function it_can_try_to_construct_with_try_from(): void
    {
        $state = CompanyState::from($value = 'STATE_DISABLED');
        $this->assertInstanceOf(CompanyState::class, $state);
        $this->assertEquals('DISABLED', $state->name);
        $this->assertEquals($value, $state->value);
    }

    /** @test */
    public function it_can_try_to_construct_from_unknown_value(): void
    {
        $state = CompanyState::tryFrom('UNKNOWN_STATE');
        $this->assertNull($state);
    }

    /** @test */
    public function it_knows_when_it_equals_states(): void
    {
        $state = CompanyState::DISABLED;
        $this->assertTrue($state->equals(CompanyState::DISABLED));
        $this->assertFalse($state->equals(CompanyState::PENDING));
    }

    /** @test */
    public function it_knows_available_states(): void
    {
        $this->assertEquals([
            'PENDING',
            'CONFIRMED',
            'STATE_DISABLED',
        ], CompanyState::availableStates());
    }
}
