<?php
declare(strict_types=1);

namespace Aymm\Tests\Unit\Model;

use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Aymm\Model\CompanyState;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class CompanyTest extends TestCase
{
    private Company $company;

    protected function setUp(): void
    {
        $this->company = Company::create(
            CompanyId::fromString('b722541e-dbff-4e05-a1ba-84887a97d15f'),
            'IT Crowd',
            CompanyState::PENDING
        );
    }

    /** @test */
    public function it_contains_an_id(): void
    {
        $this->assertInstanceOf(CompanyId::class, $this->company->companyId);
        $this->assertEquals('b722541e-dbff-4e05-a1ba-84887a97d15f', $this->company->companyId->id());
    }

    /** @test */
    public function it_contains_a_name(): void
    {
        $this->assertEquals('IT Crowd', $this->company->name);
    }

    /** @test */
    public function it_contains_an_active_flag(): void
    {
        $this->assertFalse($this->company->active);
    }

    /** @test */
    public function it_contains_a_company_state(): void
    {
        $this->assertInstanceOf(CompanyState::class, $this->company->companyState);
        $this->assertTrue(
            $this->company->companyState->equals(CompanyState::PENDING)
        );
    }

    /** @test */
    public function it_can_immutable_update_name(): void
    {
        $result = $this->company->updateName(
            $newName = 'The IT Crowd'
        );

        $this->assertInstanceOf(Company::class, $result);
        $this->assertNotSame($this->company, $result);
        $this->assertEquals('b722541e-dbff-4e05-a1ba-84887a97d15f', $this->company->companyId->id());
        $this->assertEquals($newName, $result->name);
        $this->assertFalse($result->active);
        $this->assertEquals(CompanyState::PENDING, $result->companyState);
    }

    /** @test */
    public function it_can_immutable_active(): void
    {
        $result = $this->company->activate(
            $newState = CompanyState::CONFIRMED
        );

        $this->assertInstanceOf(Company::class, $result);
        $this->assertNotSame($this->company, $result);
        $this->assertEquals('b722541e-dbff-4e05-a1ba-84887a97d15f', $this->company->companyId->id());
        $this->assertEquals('IT Crowd', $result->name);
        $this->assertTrue($result->active);
        $this->assertTrue(
            $result->companyState->equals($newState)
        );
    }
}
