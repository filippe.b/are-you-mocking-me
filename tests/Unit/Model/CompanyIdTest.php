<?php

declare(strict_types=1);

namespace Aymm\Tests\Unit\Model;

use Aymm\Model\CompanyId;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

final class CompanyIdTest extends TestCase
{
    private CompanyId $model;

    protected function setUp(): void
    {
        $this->model = CompanyId::fromString('836d4db4-4b71-4f9d-ba4c-4fcff38ac7f1');
    }

    /** @test */
    public function it_contains_an_id(): void
    {
        $this->assertEquals('836d4db4-4b71-4f9d-ba4c-4fcff38ac7f1', $this->model->id());
    }

    /** @test */
    public function it_can_be_generated(): void
    {
        $result = CompanyId::generate();
        $this->assertInstanceOf(CompanyId::class, $result);
        $this->assertTrue(Uuid::isValid($result->id()));
    }
}
