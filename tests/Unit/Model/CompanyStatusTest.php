<?php
declare(strict_types=1);

namespace Aymm\Tests\Unit\Model;

use Aymm\Model\CompanyStatus;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

final class CompanyStatusTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function it_contains_a_status(): void
    {
        $state = CompanyStatus::pending();
        $this->assertEquals('PENDING', $state->status());
    }

    /** @test */
    public function it_contains_a_value(): void
    {
        $state = CompanyStatus::confirmed();
        $this->assertEquals('CONFIRMED', $state->status());
    }

    /** @test */
    public function it_can_be_constructed_with_load(): void
    {
        $state = CompanyStatus::load($value = 'STATE_DISABLED');
        $this->assertInstanceOf(CompanyStatus::class, $state);
        $this->assertEquals('STATE_DISABLED', $state->status());
    }

    /** @test */
    public function it_cannot_be_loaded_from_unknown_value(): void
    {
        $this->expectError();
        $this->expectErrorMessage('"UNKNOWN_STATE" is not a valid backing value');

        CompanyStatus::load('UNKNOWN_STATE');
    }

    /** @test */
    public function it_knows_when_it_equals_states(): void
    {
        $state = CompanyStatus::disabled();
        $this->assertTrue($state->equals(CompanyStatus::disabled()));
        $this->assertFalse($state->equals(CompanyStatus::pending()));
    }

    /** @test */
    public function it_knows_available_states(): void
    {
        $this->assertEquals([
            'PENDING',
            'CONFIRMED',
            'STATE_DISABLED',
        ], CompanyStatus::availableStates());
    }
}
