<?php

namespace Aymm\Tests\Unit\Informer;

use Aymm\Database\Repository\CompanyRepositoryInterface;
use Aymm\Informer\CompanyInformer;
use Aymm\Informer\CompanyInformerInterface;
use Aymm\Informer\CompanyMailer;
use Aymm\Informer\InformerResult;
use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Aymm\Model\CompanyState;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

final class CompanyInformerTest extends TestCase
{
    use ProphecyTrait;

    private ObjectProphecy|CompanyRepositoryInterface $companyRepository;
    private ObjectProphecy|CompanyMailer $mailer;
    private ObjectProphecy|LoggerInterface $logger;

    private CompanyInformer $companyInformer;

    protected function setUp(): void
    {
        $this->companyRepository = $this->prophesize(CompanyRepositoryInterface::class);
        $this->mailer = $this->prophesize(CompanyMailer::class);
        $this->logger = $this->prophesize(LoggerInterface::class);

        $this->companyInformer = new CompanyInformer(
            $this->companyRepository->reveal(),
            $this->mailer->reveal(),
            $this->logger->reveal()
        );
    }

    /** @test */
    public function it_is_a_company_informer(): void
    {
        $this->assertInstanceOf(CompanyInformerInterface::class, $this->companyInformer);
    }

    /** @test */
    public function it_can_inform(): void
    {
        // setup our data
        $companyId = CompanyId::fromString('221780e3-c744-4853-8913-be9f6fb1b38a');
        $company = Company::create(
            $companyId,
            'IT-Crowd',
            CompanyState::CONFIRMED
        );

        // validate promises and predictions
        $this->logger->critical(Argument::cetera())->shouldNotBeCalled();

        $this->companyRepository->fetchById($companyId)->willReturn($company);

        $this->mailer->send($company)->shouldBeCalled();

        // argument testing
        $this->logger->debug(
            Argument::that(
                static function (string $message) {
                    return 'Mailed Jen the internet' === $message;
                }
            ),
            []
        )->shouldBeCalled();

        // run the implementation
        $result = $this->companyInformer->__invoke($companyId);

        // result testing
        $this->assertInstanceOf(InformerResult::class, $result);
        $this->assertTrue($result->success);
        $this->assertSame($company, $result->company);
    }
}
