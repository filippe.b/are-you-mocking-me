<?php

declare(strict_types=1);

namespace Aymm\Tests\Unit\Collection;

use Aymm\Collection\EmployeeIdCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class EmployeeIdCollectionTest extends TestCase
{
    private EmployeeIdCollection $collection;

    protected function setUp(): void
    {
        $this->collection = new EmployeeIdCollection();
    }

    /** @test */
    public function it_is_an_iterator(): void
    {
        $this->assertInstanceOf(\IteratorAggregate::class, $this->collection);
        $this->assertEquals([], iterator_to_array($this->collection->getIterator()));
    }

    /** @test */
    public function it_can_count(): void
    {
        $this->assertInstanceOf(\Countable::class, $this->collection);
        $this->assertCount(0, $this->collection);
    }

    /** @test */
    public function it_can_map_employee_ids(): void
    {
        // 1. setup data to be used in the test case
        $collection = new EmployeeIdCollection(
        //         Uuid::uuid4(), // bad practice
            Uuid::fromString($idOne = 'b619b6e5-bdac-4d12-a2a0-a1520b9c7401'),
            Uuid::fromString($idTwo = '8518777c-371d-4e2d-a263-2f611a654155'),
        );

        $mapper = static fn(UuidInterface $employeeId): string => $employeeId->toString();
        // 2. configure the mocked service if required
        // no mocked data needed

        // 3. call the method / case we want to test
        $result = $collection->map($mapper);

        // 4. verify (assert) the result is as expected
        $this->assertEquals(
            [
                $idOne,
                $idTwo,
            ],
            $result
        );
    }

    /** @test */
    public function it_can_filter_employee_ids(): void
    {
        $collection = new EmployeeIdCollection(
            $idOne = Uuid::fromString('b7c7d3a7-c212-4a6b-ab5c-0ef95e058fb6'),
            $idTwo = Uuid::fromString('1ebbf33a-09e9-42d0-95f7-1162f8630f55'),
            $idThree = Uuid::fromString('babe569e-c10d-4541-a028-12f553de2b27'),
        );

        $result = $collection->filter(
            static fn(UuidInterface $employeeId): bool => str_starts_with($employeeId->toString(), 'b')
        );

        $this->assertInstanceOf(EmployeeIdCollection::class, $result);

        $this->assertEquals(
            [
                $idOne,
                $idThree,
            ],
            iterator_to_array($result)
        );
    }
}
