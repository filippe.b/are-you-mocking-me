<?php

declare(strict_types=1);

namespace Aymm\Tests\Fixtures\Model;

use Aymm\Model\Employee;
use Ramsey\Uuid\Uuid;

final class EmployeeFixture
{
    public const EMMA_EMPLOYEE_ID = 'ea02fc4b-1650-47aa-b1a5-4af1ab5144df';

    public static function default(
        string $id
    ): Employee {
        return Employee::load(
            Uuid::fromString($id),
            true,
            'default-first-name',
            'default-last-name',
            new \DateTimeImmutable(),
            null
        );
    }

    public static function employee(): Employee
    {
        return Employee::load(
            Uuid::fromString(self::EMMA_EMPLOYEE_ID),
            true,
            'Emma',
            'Employee',
            new \DateTimeImmutable('-150 days 06:30:15.871'),
            new \DateTimeImmutable('-50 days 11:37:54.438')
        );
    }
}
