<?php

declare(strict_types=1);

namespace Aymm\Tests\Fixtures\Model;

use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Aymm\Model\CompanyState;

final class CompanyIdFixture
{
    private const IMPORT_COMPANY_ID = '7a1a9d78-0c22-4896-9e39-e8d182a8cde2';

    public static function default(
        string $companyId,
        ?string $name = null,
        ?bool $active = null,
        ?CompanyState $state,
    ): Company {
        return Company::load(
            CompanyId::fromString($companyId),
            $name ?? 'name-default',
            $active ?? true,
            $state ?? CompanyState::CONFIRMED
        );
    }

    /** Case Specific to import with actual import data */
    public static function forImport(): Company
    {
        return Company::load(
            CompanyId::fromString('3dc011a5-4b31-47d7-931c-eb1b0058c975'),
            'Stillman L.E.B.',
            false,
            CompanyState::PENDING
        );
    }

    /** case for storing "new" data into a repository */
    public static function forInsert(
        string $companyId
    ): Company {
        return Company::load(
            CompanyId::fromString($companyId),
            'insert-default',
            false,
            CompanyState::PENDING
        );
    }

    /** case for storing "existing" data into a repository */
    public static function forUpdate(
        string $companyId
    ): Company {
        return Company::load(
            CompanyId::fromString($companyId),
            'update-default',
            true,
            CompanyState::CONFIRMED
        );
    }

    /** Case specific to API call with actual data (used for integration testing of the api) */
    public static function SyncToApi(): Company
    {
        return Company::load(
            CompanyId::fromString('e8fa8c31-6ca2-4bc2-88a3-451fe9fb21b7'),
            'Fritsch, Herman and Nader',
            true,
            CompanyState::CONFIRMED
        );
    }
}
