<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeTwoCalamityWorkshop\Model;

use \Aymm\Model\CompanyStatus;
use PHPUnit\Framework\TestCase;

class CompanyStatusTest extends TestCase
{
    /** @test */
    public function it_is_a_calamity(): void
    {
        $this->markTestIncomplete('// TODO: Silence before the storm.');
    }
}
