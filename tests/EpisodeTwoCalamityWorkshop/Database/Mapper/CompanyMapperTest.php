<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeTwoCalamityWorkshop\Database\Mapper;

use Aymm\Database\Mapper\CompanyMapper;
use PHPUnit\Framework\TestCase;

class CompanyMapperTest extends TestCase
{
    /** @test */
    public function it_is_a_calamity(): void
    {
        $this->markTestIncomplete('// TODO: Silence before the storm.');
    }
}
