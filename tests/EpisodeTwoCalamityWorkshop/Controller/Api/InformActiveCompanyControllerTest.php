<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeTwoCalamityWorkshop\Controller\Api;

use Aymm\Controller\Api\InformActiveCompanyController;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class InformActiveCompanyControllerTest extends TestCase
{
    use ProphecyTrait;

    protected function setUp(): void
    {
        $this->controller = $this->prophesize(InformActiveCompanyController::class);
    }


    /** @test */
    public function it_is_a_calamity(): void
    {
        $this->markTestIncomplete('// TODO: Silence before the storm.');
    }
}
