<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeTwoCalamityWorkshop\Informer;

use Aymm\Informer\CompanyInformer;
use PHPUnit\Framework\TestCase;

class CompanyInformerTest extends TestCase
{
    /** @test */
    public function it_is_a_calamity(): void
    {
        $this->markTestIncomplete('// TODO: Silence before the storm.');
    }
}
