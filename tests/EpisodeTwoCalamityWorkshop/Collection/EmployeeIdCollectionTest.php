<?php

declare(strict_types=1);

namespace Aymm\Tests\EpisodeTwoCalamityWorkshop\Collection;

use Aymm\Collection\EmployeeIdCollection;
use PHPUnit\Framework\TestCase;

class EmployeeIdCollectionTest extends TestCase
{
    /** @test */
    public function it_is_a_calamity(): void
    {
        $this->markTestIncomplete('// TODO: Silence before the storm.');
    }
}
