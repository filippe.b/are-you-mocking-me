<?php

declare(strict_types=1);

namespace Aymm\Database\Repository;

use Aymm\Collection\CompanyCollection;
use Aymm\Model\Company;
use Aymm\Model\CompanyId;

interface CompanyRepositoryInterface
{
    public function fetchById(CompanyId $companyId): Company;

    public function fetchActive(): CompanyCollection;
}
