<?php

declare(strict_types=1);

namespace Aymm\Database\Repository;

use Aymm\Collection\CompanyCollection;
use Aymm\Database\Mapper\CompanyMapperInterface;
use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Doctrine\DBAL\Connection;
use Aymm\Database\Connection as AymmConnection;

final class CompanyRepository implements CompanyRepositoryInterface
{
    private Connection|AymmConnection $connection;
    private CompanyMapperInterface $companyMapper;

    public function __construct(Connection|AymmConnection $connection, CompanyMapperInterface $companyMapper)
    {
        $this->connection = $connection;
        $this->companyMapper = $companyMapper;
    }

    public function fetchById(CompanyId $companyId): Company
    {
        return $this->companyMapper->convertFromDb(
            $this->connection->fetchAssociative(
                'SELECT * FROM company WHERE id = :companyId;',
                ['companyId' => $companyId->id()]
            )
        );
    }

    public function fetchActive(): CompanyCollection
    {
        $result = $this->connection->fetchAllAssociative('SELECT * FROM company WHERE active = TRUE;');

        if (!count($result)) {
            return new CompanyCollection();
        }

        return new CompanyCollection(...array_map(
            fn(array $data): Company => $this->companyMapper->convertFromDb($data),
            $result
        ));
    }

    public function create(Company $company): void
    {
        $this->connection->insert('company', $this->companyMapper->convertToDb($company));
    }
}
