<?php

declare(strict_types=1);

namespace Aymm\Database\Repository;

use Aymm\Collection\BrandCollection;
use Aymm\Database\Mapper\BrandMapper;
use Aymm\Model\Brand;
use Aymm\Model\BrandId;
use Doctrine\DBAL\Connection;

/* final */ class BrandRepository
{
    private const TABLE = 'brand';

    public function __construct(
        private readonly Connection $connection,
        private readonly BrandMapper $mapper,
    ) {
    }


    public function fetchById(BrandId $brandId): Brand
    {
        $record = $this->connection->fetchAssociative(
            'SELECT * FROM '.self::TABLE.' WHERE id = "'.$brandId->id().'"'
        );

        return $this->mapper->fromDb($record);
    }

    public function fetchLastUpdated(): BrandCollection
    {
        $records = $this->connection->fetchAllAssociative(
            'SELECT * FROM '.self::TABLE.' ORDER BY updated_at DESC LIMIT 5'
        );

        return new BrandCollection(...\array_map(
            fn(array $record): Brand => $this->mapper->fromDb($record),
            $records
        ));
    }

    public function create(Brand $brand): void
    {
        $this->connection->insert(self::TABLE, $this->mapper->toDb($brand));
    }


    public function update(Brand $brand): void
    {
        $this->connection->update(self::TABLE, $this->mapper->toDb($brand), [
            'id' => $brand->brandId->id(),
        ]);
    }
}