<?php

declare(strict_types=1);

namespace Aymm\Database\Provider\Employee;

use Aymm\Collection\EmployeeIdCollection;
use Doctrine\DBAL\Connection;

final class ActiveItDepartmentIdProvider implements EmployeeIdProvider
{
    public function __construct(
        private Connection $connection
    ) {
    }

    public function provide(): EmployeeIdCollection
    {
        return new EmployeeIdCollection();
    }
}
