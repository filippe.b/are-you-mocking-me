<?php

declare(strict_types=1);

namespace Aymm\Database\Provider\Employee;

use Aymm\Collection\EmployeeIdCollection;
use Doctrine\DBAL\Connection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class ActiveIdProvider implements EmployeeIdProvider
{
    public function __construct(
        private Connection $connection
    ) {
    }

    public function provide(): EmployeeIdCollection
    {
        return new EmployeeIdCollection(...array_map(
            static fn(string $id): UuidInterface => Uuid::fromString($id),
            $this->connection->fetchFirstColumn('SELECT id FROM employee WHERE active = 1')
        ));
    }
}
