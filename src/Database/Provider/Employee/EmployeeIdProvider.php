<?php

declare(strict_types=1);

namespace Aymm\Database\Provider\Employee;

use Aymm\Collection\EmployeeIdCollection;

interface EmployeeIdProvider
{
    public function provide(): EmployeeIdCollection;
}
