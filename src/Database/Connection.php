<?php

declare(strict_types=1);

namespace Aymm\Database;

/**
 * @deprecated
 */
class Connection
{
    public function insert($table, array $data): void
    {

    }

    public function fetchById(string $id): array
    {
        return [];
    }

    public function fetchAssociative(string $id): array
    {
        return [];
    }

    public function fetchAllAssociative(string $id): array
    {
        return [];
    }
}
