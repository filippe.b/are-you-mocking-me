<?php

declare(strict_types=1);

namespace Aymm\Database\Mapper;

use Aymm\Model\Brand;
use Aymm\Model\BrandId;

class BrandMapper
{
    public function toDb(Brand $brand): array
    {
        return [
            'id' => $brand->brandId->id(),
            'uid' => $brand->uid,
            'name' => $brand->name,
            'updated_at' => $brand->updatedAt?->format('Y-m-d H:i:s'),
        ];
    }

    public function fromDb(array $data): Brand
    {
        return Brand::load(
            BrandId::fromString($data['id']),
            $data['uid'],
            $data['name'],
            null !== $data['updated_at'] ? new \DateTimeImmutable($data['updated_at']) : null,
        );
    }
}
