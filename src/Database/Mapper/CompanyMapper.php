<?php

declare(strict_types=1);

namespace Aymm\Database\Mapper;

use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Aymm\Model\CompanyState;

final class CompanyMapper implements CompanyMapperInterface
{
    public function convertToDb(Company $company): array
    {
        return [
            'id' => $company->companyId->id(),
            'name' => $company->name,
            'active' => (int) $company->active,
            'state' => $company->companyState->status(),
        ];
    }

    public function convertFromDb(array $data): Company
    {
        return Company::load(
            CompanyId::fromString($data['id']),
            $data['name'] ?? '',
            (bool) ($data['active'] ?? false),
            CompanyState::tryFrom($data['state'])
        );
    }
}
