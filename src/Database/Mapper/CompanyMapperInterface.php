<?php

declare(strict_types=1);

namespace Aymm\Database\Mapper;

use Aymm\Model\Company;

interface CompanyMapperInterface
{
    public function convertToDb(Company $company): array;

    public function convertFromDb(array $data): Company;
}
