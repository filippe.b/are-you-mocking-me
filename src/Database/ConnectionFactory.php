<?php

declare(strict_types=1);

namespace Aymm\Database;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;

final class ConnectionFactory
{
    private static ?Connection $connection = null;

    public static function create(): Connection
    {
        if (self::$connection) {
            return self::$connection;
        }

        return self::$connection = DriverManager::getConnection([
            'url' => 'mysql://root:toor@db/are_you_mocking_me',
        ]);
    }
}
