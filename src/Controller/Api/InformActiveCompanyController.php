<?php

declare(strict_types=1);

namespace Aymm\Controller\Api;

use Aymm\Collection\CompanyCollection;
use Aymm\Database\Repository\CompanyRepositoryInterface;
use Aymm\Http\JsonResponse;
use Aymm\Informer\CompanyInformerInterface;
use Aymm\Model\Company;
use Aymm\Serializer\CompanySerializer;

class InformActiveCompanyController
{
    public function __construct(
        private CompanyRepositoryInterface $companyRepository,
        private CompanyInformerInterface $companyInformer,
        private CompanySerializer $companySerializer
    ) {
    }

    public function __invoke(): JsonResponse
    {
        $informedCompanies = new CompanyCollection();

        $companies = $this->companyRepository->fetchActive();

        $informedResults = $this->companyInformer->informAll($companies);
        foreach ($informedResults as $company) {
            $informedCompanies->addFromInformerd($company);
        }

        return JsonResponse::success(
            $informedCompanies->map(
                fn(Company $company): array => $this->companySerializer->serialize($company)
            )
        );
    }
}
