<?php

declare(strict_types=1);

namespace Aymm\Informer;

use Aymm\Model\Company;

class InformerResult
{
    public function __construct(
        public readonly Company $company,
        public readonly bool $success,
    ){}
}
