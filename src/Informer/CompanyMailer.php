<?php

declare(strict_types=1);

namespace Aymm\Informer;

use Aymm\Model\Company;

interface CompanyMailer
{
    public function send(Company $company): void;
}
