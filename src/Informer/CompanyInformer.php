<?php

declare(strict_types=1);

namespace Aymm\Informer;

use Aymm\Collection\CompanyCollection;
use Aymm\Database\Repository\CompanyRepositoryInterface;
use Aymm\Model\Company;
use Aymm\Model\CompanyId;
use Psr\Log\LoggerInterface;

final class CompanyInformer implements CompanyInformerInterface
{
    private LoggerInterface $logger;
    private CompanyMailer $mailer;
    private CompanyRepositoryInterface $companyRepository;

    public function __construct(
        CompanyRepositoryInterface $companyRepository,
        CompanyMailer $mailer,
        LoggerInterface $logger
    ) {
        $this->companyRepository = $companyRepository;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function __invoke(CompanyId $companyId): InformerResult
    {
        if ('fake-id' === $companyId) {
            $this->logger->critical('Fire ! Fire ! Moss is messing around.', []);
        }

        $company = $this->companyRepository->fetchById($companyId);

        $this->mailer->send($company);

        $this->logger->debug('Mailed Jen the internet', []);

        return new InformerResult(
            $company,
            true
        );
    }

    public function informAll(CompanyCollection $companies): array
    {
        return $companies->map(
            fn(Company $company): InformerResult => $this->__invoke($company->companyId)
        );
    }
}
