<?php

declare(strict_types=1);

namespace Aymm\Informer;

use Aymm\Collection\CompanyCollection;
use Aymm\Model\CompanyId;

interface CompanyInformerInterface
{
    public function __invoke(CompanyId $companyId): InformerResult;

    /**
     * @return InformerResult[], array<int, InformerResult>
     */
    public function informAll(CompanyCollection $companies): array;
}
