<?php

declare(strict_types=1);

namespace Aymm\Model;

final class Brand
{
    private function __construct(
        public readonly BrandId $brandId,
        public readonly string $uid,
        public readonly string $name,
        public readonly ?\DateTimeImmutable $updatedAt
    ) {
    }

    public static function load(
        BrandId $brandId,
        string $uid,
        string $name,
        ?\DateTimeImmutable $updatedAt
    ): self {
        return new self(
            $brandId,
            $uid,
            $name,
            $updatedAt
        );
    }

    public function update(string $uid, $name): brand
    {
        return new self(
            $this->brandId,
            $uid,
            $name,
            new \DateTimeImmutable()
        );
    }
}
