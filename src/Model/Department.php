<?php

declare(strict_types=1);

namespace Aymm\Model;

use Ramsey\Uuid\UuidInterface;

final class Department
{
    public function __construct(
        public readonly UuidInterface $id,
        public readonly CompanyId $companyId,
        public readonly string $name,
        public readonly string $location,
    ) {
    }
}
