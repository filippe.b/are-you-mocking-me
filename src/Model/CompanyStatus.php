<?php

declare(strict_types=1);

namespace Aymm\Model;

class CompanyStatus implements State
{
    private const PENDING = 'PENDING';
    private const CONFIRMED = 'CONFIRMED';
    private const DISABLED = 'STATE_DISABLED';

    private string $status;

    private function __construct(string $status)
    {
        if (!in_array($status, self::availableStates())) {
            throw new \Error('"'.$status.'" is not a valid backing value');
        }
        $this->status = $status;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function equals(self $status): bool
    {
        return $this->status === $status->status;
    }

    public static function load(string $status): self
    {
        return new self($status);
    }

    public static function pending(): self
    {
        return new self(self::PENDING);
    }

    public static function confirmed(): self
    {
        return new self(self::CONFIRMED);
    }

    public static function disabled(): self
    {
        return new self(self::DISABLED);
    }

    public static function availableStates(): array
    {
        return [
            self::PENDING,
            self::CONFIRMED,
            self::DISABLED,
        ];
    }
}
