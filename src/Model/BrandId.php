<?php

declare(strict_types=1);

namespace Aymm\Model;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class BrandId
{
    private function __construct(
        private readonly UuidInterface $uuid
    ) {
    }

    public static function fromString(string $uuid): self
    {
        return new self(Uuid::fromString($uuid));
    }

    public function id(): string
    {
        return $this->uuid->toString();
    }
}
