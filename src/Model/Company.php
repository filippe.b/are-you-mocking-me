<?php

declare(strict_types=1);

namespace Aymm\Model;

final class Company
{
    public readonly CompanyId $companyId;
    public readonly string $name;
    public readonly bool $active;
    public readonly State $companyState;

    private function __construct()
    {
    }

    public static function create(CompanyId $id, string $name, State $companyStatus): self
    {
        $new = new self();
        $new->companyId = $id;
        $new->name = $name;
        $new->active = false;
        $new->companyState = $companyStatus;

        return $new;
    }

    public static function load(CompanyId $id, string $name, bool $active, State $companyStatus): self
    {
        $new = new self();
        $new->companyId = $id;
        $new->name = $name;
        $new->active = $active;
        $new->companyState = $companyStatus;

        return $new;
    }

    public function updateName(string $name): self
    {
        return $this->clone(
            name: $name
        );
    }

    public function activate(State $companyState): self
    {
        return $this->clone(
            active: true,
            companyStatus: $companyState,
        );
    }

    private function clone(
        CompanyId $companyId = null,
        string $name = null,
        bool $active = null,
        State $companyStatus = null
    ): self {
        $clone = new self();
        $clone->companyId = $companyId ?? $this->companyId;
        $clone->name = $name ?? $this->name;
        $clone->active = $active ?? $this->active;
        $clone->companyState = $companyStatus ?? $this->companyState;

        return $clone;
    }
}
