<?php

declare(strict_types=1);

namespace Aymm\Model;

interface State
{
    public function status(): string;
}
