<?php

declare(strict_types=1);

namespace Aymm\Model;

use Ramsey\Uuid\UuidInterface;

final class Employee
{
    private UuidInterface $id;
    private bool $active;
    private string $firstName;
    private string $lastName;
    private \DateTimeInterface $createdAt;
    private ?\DateTimeInterface $updatedAt;
    private ?Department $department;

    private function __construct()
    {
    }

    public static function create(
        UuidInterface $id,
        bool $active,
        string $firstName,
        string $lastName
    ) {
        $new = new self();
        $new->id = $id;
        $new->active = $active;
        $new->firstName = $firstName;
        $new->lastName = $lastName;
        $new->createdAt = new \DateTimeImmutable();
        $new->updatedAt = null;
        $new->department = null;

        return $new;
    }

    public static function load(
        UuidInterface $id,
        bool $active,
        string $firstName,
        string $lastName,
        \DateTimeInterface $createdAt,
        ?\DateTimeInterface $updatedAt
    ) {
        $new = new self();
        $new->id = $id;
        $new->active = $active;
        $new->firstName = $firstName;
        $new->lastName = $lastName;
        $new->createdAt = $createdAt;
        $new->updatedAt = $updatedAt;
        $new->department = null;

        return $new;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function active(): bool
    {
        return $this->active;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function createdAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function updatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }
    public function department(): ?Department
    {
        return $this->department;
    }

    public function update(string $firstName, string $lastName): self
    {
        $new = clone $this;
        $new->firstName = $firstName;
        $new->lastName = $lastName;
        $new->updatedAt = new \DateTimeImmutable();

        return $new;
    }

    public function withDepartment(Department $department): self
    {
        $new = clone $this;
        $new->department = $department;

        return $new;
    }
}
