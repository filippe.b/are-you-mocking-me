<?php

declare(strict_types=1);

namespace Aymm\Model;

enum CompanyState: string implements State
{
    case PENDING = 'PENDING';
    case CONFIRMED = 'CONFIRMED';
    case DISABLED = 'STATE_DISABLED';

    public function state(): string
    {
        return $this->value;
    }

    public function status(): string
    {
        return $this->value;
    }

    public function equals(self $status): bool
    {
        return $this->value === $status->value;
    }

    public static function availableStates(): array
    {
        return [
            self::PENDING->value,
            self::CONFIRMED->value,
            self::DISABLED->value,
        ];
    }
}
