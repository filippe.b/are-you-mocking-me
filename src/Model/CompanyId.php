<?php

declare(strict_types=1);

namespace Aymm\Model;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/* final */ class CompanyId
{
    private function __construct(
        private readonly UuidInterface $uuid
    ) {
    }

    public static function fromString(string $uuid): self
    {
        return new self(Uuid::fromString($uuid));
    }

    public static function generate(): self
    {
        return new self(Uuid::uuid4());
    }

    public function id(): string
    {
        return $this->uuid->toString();
    }
}
