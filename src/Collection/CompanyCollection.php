<?php

declare(strict_types=1);

namespace Aymm\Collection;

use Aymm\Informer\InformerResult;
use Aymm\Model\Company;

final class CompanyCollection implements \Countable, \IteratorAggregate
{
    /**
     * @var Company[]|array<int, Company>
     */
    private array $companies;

    public function __construct(Company ...$companies)
    {
        $this->companies = $companies;
    }

    /**
     * @return Company[] | \Traversable<int, Company>
     */
    public function getIterator(): \Traversable
    {
        yield from $this->companies;
    }

    public function count(): int
    {
        return count($this->companies);
    }

    public function addFromInformerd(InformerResult $result): self
    {
        if (!$result->success) {
            return $this;
        }

        $companies = [...$this->companies];
        $companies[] = $result->company;

        return new self(...$companies);
    }

    public function filter(callable $filter): self
    {
        return new self(...array_filter($this->companies, $filter));
    }

    public function filterActive(): self
    {
        return new self(...array_filter(
            $this->companies,
            static fn(Company $company): bool => $company->active
        ));
    }

    public function map(callable $mapper): array
    {
        return array_map($mapper, $this->companies);
    }
}
