<?php

declare(strict_types=1);

namespace Aymm\Collection;

use Aymm\Model\Brand;

final class BrandCollection implements \Countable, \IteratorAggregate
{
    private array $brands;

    public function __construct(Brand ...$brands)
    {
        $this->brands = $brands;
    }

    /**
     * @return \Traversable<Brand>
     */
    public function getIterator(): \Traversable
    {
        yield from $this->brands;
    }

    public function count(): int
    {
        return \count($this->brands);
    }
}