<?php

declare(strict_types=1);

namespace Aymm\Collection;

use Ramsey\Uuid\UuidInterface;

final class EmployeeIdCollection implements \Countable, \IteratorAggregate
{
    /**
     * @var array<int, UuidInterface>|UuidInterface[]
     */
    private array $employeeIds;

    public function __construct(UuidInterface ...$employeeIds)
    {
        $this->employeeIds = $employeeIds;
    }

    public function getIterator(): \Traversable
    {
        yield from $this->employeeIds;
    }

    public function count(): int
    {
        return count($this->employeeIds);
    }

    public function map(callable $mapper): array
    {
        return array_map($mapper, $this->employeeIds);
    }

    public function filter(callable $filter): self
    {
        return new self(...array_filter($this->employeeIds, $filter));
    }
}
