<?php

declare(strict_types=1);

namespace Aymm\Http;

final class JsonResponse
{
    private function __construct(
        public readonly HttpCode $statusCode,
        public readonly array $data,
    ) {
    }

    public static function create(HttpCode $statusCode, array $data): self
    {
        return new self($statusCode, $data);
    }

    public static function success(array $data): self
    {
        return new self(HttpCode::OK, $data);
    }
}
