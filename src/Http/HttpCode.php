<?php

declare(strict_types=1);

namespace Aymm\Http;

enum HttpCode: int
{
    case OK = 200;
    case BAD_REQUEST = 400;
    case TEAPOT = 418;
}
