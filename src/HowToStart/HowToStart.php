<?php

declare(strict_types=1);

namespace Aymm\HowToStart;

use Psr\Log\LoggerInterface;

final class HowToStart
{
    private string $name;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, string $name)
    {
        $this->logger = $logger;
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function logName(): string
    {
        if ('' === $this->name) {
            return '';
        }

        if ('fake' === $this->name) {
            throw new \InvalidArgumentException('Invalid name: "'.$this->name.'"');
        }

        $message = 'Name: '.$this->parseName();
        $this->logger->info($message);

        return $message;
    }

    private function parseName(): string
    {
        return ucwords(strtolower($this->name));
    }
}
