<?php

declare(strict_types=1);

namespace Aymm\Serializer;

use Aymm\Model\Company;

interface CompanySerializer
{
    public function serialize(Company $company): array;
}
