<?php

declare(strict_types=1);

namespace Aymm\Serializer;

use Aymm\Model\Company;

final class CompanyInformedSerializer implements CompanySerializer
{
    public function serialize(Company $company): array
    {
        return [
            'id' => $company->companyId,
            'name' => $company->name,
        ];
    }
}
