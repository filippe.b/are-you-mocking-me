ALTER SCHEMA `aymm` DEFAULT COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `company`
(
    `id`     VARCHAR(36) NOT NULL,
    `name`   VARCHAR(50) NOT NULL DEFAULT '',
    `active` BOOLEAN     NOT NULL DEFAULT TRUE,
    `state`  VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `department`
(
    `id`         VARCHAR(36) NOT NULL,
    `company_id` VARCHAR(36) NOT NULL,
    `name`       VARCHAR(50) NOT NULL DEFAULT '',
    `location`   VARCHAR(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `employee`
(
    `id`         VARCHAR(36) NOT NULL,
    `active`     BOOLEAN     NOT NULL DEFAULT TRUE,
    `first_name` VARCHAR(50) NOT NULL DEFAULT '',
    `last_name`  VARCHAR(50) NOT NULL DEFAULT '',
    `created_at` DATETIME    NOT NULL,
    `updated_at` DATETIME             DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `brand`
(
    `id`         VARCHAR(36) NOT NULL,
    `uid`        VARCHAR(50) NOT NULL DEFAULT '',
    `name`       VARCHAR(50) NOT NULL DEFAULT TRUE,
    `updated_at` DATETIME             DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
DEFAULT CHARSET = utf8mb4
COLLATE = utf8mb4_unicode_ci;